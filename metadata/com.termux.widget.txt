Categories:System
License:GPLv3
Web Site:https://termux.com
Source Code:https://github.com/termux/termux-widget
Issue Tracker:https://github.com/termux/termux-widget/issues
Donate:https://termux.com/donate.html
Bitcoin:1BXS5qPhJzhr5iK5nmNDSmoLDfB6VmN5hv

Auto Name:Termux:Widget
Summary:Launch Termux commands from the homescreen
Description:
Add-on app which adds shortcuts to [[com.termux]] scripts and commands on the
home screen. Scripts should be placed in the $HOME/.shortcuts/ folder to allow
quick access to frequently used commands without typing.
.

Repo Type:git
Repo:https://github.com/termux/termux-widget

Build:0.3,3
    commit=v0.3
    subdir=app
    gradle=yes

Build:0.4,4
    disable=pre-release

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.3
Current Version Code:3
